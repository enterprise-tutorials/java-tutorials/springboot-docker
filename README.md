# Springboot with Docker
This example is to dockerize a Springboot application, launch it and even push to DockerHub.

## Pre-requsites
* [Java 1.8 or later](https://www.oracle.com/technetwork/java/javase/downloads/index.html)
* [Docker](https://www.docker.com/)

Optional
* [Gradle 4+](https://gradle.org/install/). This is required only if you want to rebuild the gradle wrapper.

## Steps to Start the app
There are 2 ways to start the app:

### A. Launching without Docker
1. `cd` into the project directory
2. (Optional)`gradle wrapper` will generate the gradle* files. Typically you do this only once. The gradle files are already present in this repo, so you can skip this command.
3. `./gradlew clean build`
4. `java -jar build/libs/springboot-docker-0.1.0.jar`

Now open a browser and go to http://localhost:8080/.

**Misc**
* To run your tests - `./gradlew test`


### B. Launching with Docker
1. `docker build -t springboot-docker .`
2. Check if the image is present on your system - `docker image ls`
3. Run the docker image:

...In Foreground: `docker run -p 5000:8080 springboot-docker`

...In Background: `docker run -d -p 5000:8080 springboot-docker`

Now open a browser and go to http://localhost:5000.

### B1. Pushing to DockerHub
1. Make sure you have logged in to the correct account - `docker login`
2. Tag the image - `docker tag springboot-docker codercheerful/springboot-docker:0.0.1-SNAPSHOT`
3. Check to see the new image - `docker image ls`
4. Push the image - `docker push codercheerful/springboot-docker:0.0.1-SNAPSHOT`
5. Rull image from Dockerhub & run it `docker run -p 5000:8080 codercheerful/springboot-docker:0.0.1-SNAPSHOT`

### C. To stop the app IF you have launched using docker
1. `docker ps` or `docker container ls` to get the CONTAINER ID of this app
2. `docker stop put_container_ID` to stop
3. `docker rm put_container_ID` to remove the container from docker registry


## References
* [Spring.io Guides](https://spring.io/guides/gs/spring-boot-docker/)
* [Callicoder](https://www.callicoder.com/spring-boot-docker-example/)

## Questions/Comments/Feedbacks/Concerns
[Email](codercheerful@gmail.com)
